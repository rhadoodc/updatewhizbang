﻿using Sprocket.UpdateMonitor.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Sprocket.UpdateMonitor
{
	public partial class SyncItemProperties : Form
	{
		private List<Button> browseButtons = new List<Button>();
		private List<Button> removeTargetButtons = new List<Button>();
		private List<Label> targetLabels = new List<Label>();
		public List<TextBox> targetPathTextBoxes = new List<TextBox>();

		const string invalidTargetPath_key = "The target path is invalid: {0}";

		const string targetPathLabel_key = "Target [{0}]";

		const string confirmRemoveTargetTitle_Key = "Remove Target";
		const string confirmRemoveTarget_Key = "Are you sure you want to remove this target?\n\n    {0}";

		const int controlLineSize = 24;
		const int controlLineMargin = 5;

		readonly Size browseButtonSize = new Size(controlLineSize, controlLineSize);
		readonly Size removeTargetButtonSize = new Size(controlLineSize, controlLineSize);
		readonly Size targetTextBoxSize = new Size(220, controlLineSize);
		readonly Size targetLabelSize = new Size(55, controlLineSize);

		const int browseButtonXOffset = 290;
		const int removeTargetButtonXOffset = 315;
		const int targetTextBoxXOffset = 65;
		const int targetLabelXOffset = 10;

		const int targetLayoutBaseline = 67;

		private static string lastTargetPath = "";

		public SyncItemProperties()
		{
			InitializeComponent();
		}

		private void cancelButton_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.Cancel;

			Close();
		}

		private void applyButton_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.OK;

			Close();
		}

		private void targetPathTextBox_TextChanged(object sender, EventArgs e)
		{
			this.Validate();
		}

		public void ConstructTargetField(int index)
		{
			if (index < 0)
			{
				index = targetPathTextBoxes.Count;
			}

			//target path text box
			var targetPathTextBox = new TextBox();

			targetPathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));

			targetPathTextBox.Size = targetTextBoxSize;
			targetPathTextBox.Location = new Point(targetTextBoxXOffset, targetLayoutBaseline + (controlLineSize + controlLineMargin) * index);

			targetPathTextBox.TextChanged += targetPathTextBox_TextChanged;

			targetPathTextBox.Validating += (object sender, CancelEventArgs e) =>
			{
				try
				{
					var path = Path.GetFullPath(targetPathTextBox.Text);
				}
				catch (System.Exception ex)
				{
					e.Cancel = true;

					if (applyButton.Enabled == true)
					{
						applyButton.Enabled = false;

						errorProvider.Icon = Resources.warning_icon;
						errorProvider.SetIconPadding(targetPathTextBox, 5);
						errorProvider.SetError(targetPathTextBox, string.Format(invalidTargetPath_key, ex.Message));
					}
				}
			};

			//target label
			var targetLabel = new Label();

			targetLabel.Text = string.Format(targetPathLabel_key, index);

			targetLabel.Size = targetLabelSize;
			targetLabel.Location = new Point(targetLabelXOffset, targetLayoutBaseline + 3 + (controlLineSize + controlLineMargin) * index);

			//browse button
			var browseButton = new Button();

			browseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));

			browseButton.Image = Resources.targetBrowse;
			browseButton.UseVisualStyleBackColor = true;

			browseButton.Size = browseButtonSize;
			browseButton.Location = new Point(browseButtonXOffset, targetLayoutBaseline - 2 + (controlLineSize + controlLineMargin) * index);

			browseButton.Click += (object sender, EventArgs e) => 
			{
				folderBrowserDialog.SelectedPath = (targetPathTextBox.Text == string.Empty) ? lastTargetPath : targetPathTextBox.Text;
				var result = folderBrowserDialog.ShowDialog();

				if (result == DialogResult.OK)
				{
					lastTargetPath = folderBrowserDialog.SelectedPath;
					targetPathTextBox.Text = folderBrowserDialog.SelectedPath;
				}
			};

			//remove target button
			var removeTargetButton = new Button();

			removeTargetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));

			removeTargetButton.Image = Resources.targetRemove;
			removeTargetButton.UseVisualStyleBackColor = true;

			removeTargetButton.Size = removeTargetButtonSize;
			removeTargetButton.Location = new Point(removeTargetButtonXOffset, targetLayoutBaseline - 2 + (controlLineSize + controlLineMargin) * index);

			removeTargetButton.Click += (object sender, EventArgs e) =>
			{
				if (MessageBox.Show(string.Format(confirmRemoveTarget_Key, targetPathTextBox.Text), confirmRemoveTargetTitle_Key, MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					RemoveTargetField(targetLabel);
				}
			};

			targetPathTextBoxes.Insert(index, targetPathTextBox);
			targetLabels.Insert(index, targetLabel);
			browseButtons.Insert(index, browseButton);
			removeTargetButtons.Insert(index, removeTargetButton);

			Controls.Add(targetPathTextBox);
			Controls.Add(targetLabel);
			Controls.Add(browseButton);
			Controls.Add(removeTargetButton);

			this.Size = new Size(this.Size.Width, this.Size.Height + controlLineMargin + controlLineSize);
		}

		private void RemoveTargetField(Label label)
		{
			var index = targetLabels.IndexOf(label);

			var removeTargetButton = removeTargetButtons[index];
			var browseButton = browseButtons[index];
			var targetLabel = targetLabels[index];
			var targetPathTextBox = targetPathTextBoxes[index];

			Controls.Remove(removeTargetButton);
			Controls.Remove(browseButton);
			Controls.Remove(targetLabel);
			Controls.Remove(targetPathTextBox);

			removeTargetButtons.Remove(removeTargetButton);
			browseButtons.Remove(browseButton);
			targetLabels.Remove(targetLabel);
			targetPathTextBoxes.Remove(targetPathTextBox);

			ReflowTargetFields();

			this.Size = new Size(this.Size.Width, this.Size.Height - controlLineMargin - controlLineSize);
		}

		private void ReflowTargetFields()
		{
			for (int index = 0; index < targetPathTextBoxes.Count; index ++)
			{
				var removeTargetButton = removeTargetButtons[index];
				var browseButton = browseButtons[index];
				var targetLabel = targetLabels[index];
				var targetPathTextBox = targetPathTextBoxes[index];

				targetLabel.Text = string.Format(targetPathLabel_key, index);
				targetLabel.Location = new Point(targetLabelXOffset, targetLayoutBaseline + (controlLineSize + controlLineMargin) * index);
				removeTargetButton.Location = new Point(removeTargetButtonXOffset, targetLayoutBaseline + (controlLineSize + controlLineMargin) * index);
				browseButton.Location = new Point(browseButtonXOffset, targetLayoutBaseline + (controlLineSize + controlLineMargin) * index);
				targetPathTextBox.Location = new Point(targetTextBoxXOffset, targetLayoutBaseline + (controlLineSize + controlLineMargin) * index);
			}
		}

		private void addTargetButton_Click(object sender, EventArgs e)
		{
			ConstructTargetField(-1);
		}

		private void SyncItemProperties_Shown(object sender, EventArgs e)
		{
			if (targetPathTextBoxes.Count == 0)
			{
				ConstructTargetField(-1);
			}
		}
	}
}
