﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Sprocket.UpdateMonitor
{
	public class PathMaskManager
	{
		public enum MaskMode
		{
			DisregardMasks,
			EnableOnlyMaskedPaths,
			DisableMaskedPaths,
		}

		[Serializable]
		public struct Mask
		{
			public string maskString;
			public bool isRegex;
			public bool enabled;
		}

		[NonSerialized]
		[XmlIgnore]
		private static PathMaskManager instance = null;

		public static PathMaskManager Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new PathMaskManager();
				}

				return instance;
			}
		}

		private PathMaskManager()
		{

		}

		private bool modified = false;

		public bool Modified
		{
			get
			{
				return modified;
			}
		}

		private static XmlSerializer xmlSerializer = new XmlSerializer(typeof(PathMaskManager));

		public List<Mask> masks;

		public MaskMode CurrentMode { get; set; }

		private const string pathMasksLoadError_key = "An error occured trying to load path masks from '{0}':\n{1}";
		private const string pathMasksLoadErrorTitle_key = "Couldn't load path masks...";
		private const string pathMasksSaveError_key = "An error occured trying to save path masks to '{0}':\n{1}";
		private const string pathMasksSaveErrorTitle_key = "Couldn't save path masks...";

		public void SetMasks(List<Mask> _masks)
		{
			modified = true;
			masks = _masks;
		}

		public static string GetUserFriendlyName(string input)
		{
			return Regex.Replace(input, @"(?<a>(?<!^)((?:[A-Z][a-z])|(?:(?<!^[A-Z]+)[A-Z0-9]+(?:(?=[A-Z][a-z])|$))|(?:[0-9]+)))", @" ${a}");
		}

		public bool IsTargetAllowed(string target)
		{
			if (CurrentMode == MaskMode.DisregardMasks)
				return true;

			foreach (var mask in masks)
			{
				if (mask.enabled && (mask.maskString != string.Empty) && ((mask.isRegex && (Regex.Match(target, mask.maskString) != Match.Empty))
				|| (target.Contains(mask.maskString))))
				{
					if (CurrentMode == MaskMode.DisableMaskedPaths)
						return false;
					else //if (currentMode == MaskMode.EnableOnlyMaskedPaths) -- implicit
						return true;
				}
			}

			if (CurrentMode == MaskMode.DisableMaskedPaths)
				return true;
			else //if (currentMode == MaskMode.EnableOnlyMaskedPaths) -- implicit
				return false;
		}

		internal static void LoadMasks(string PathMasksPath)
		{
			//Instance.Modified = false;

			TextReader reader = null;

			try
			{
				reader = new StreamReader(PathMasksPath);

				//masks = (List<Mask>)xmlSerializer.Deserialize(reader);
				instance = (PathMaskManager)xmlSerializer.Deserialize(reader);
			}
			catch (System.Exception ex)
			{
				if (!(ex is FileNotFoundException || ex is DirectoryNotFoundException))
				{
					Program.Log(string.Format(pathMasksLoadError_key, PathMasksPath, ex.Message),
								pathMasksLoadErrorTitle_key);

					MessageBox.Show(string.Format(pathMasksLoadError_key, PathMasksPath, ex.Message),
								pathMasksLoadErrorTitle_key,
								MessageBoxButtons.OK,
								MessageBoxIcon.Error,
								MessageBoxDefaultButton.Button1);
				}
			}
			finally
			{
				if (reader != null)
					reader.Close();
			}
		}

		internal static void SaveMasks(string PathMasksPath)
		{
			Instance.modified = false;

			TextWriter writer = null;

			try
			{
				var s = Path.GetDirectoryName(PathMasksPath);
				Directory.CreateDirectory(s);
				writer = new StreamWriter(PathMasksPath);
				xmlSerializer.Serialize(writer, instance);
			}
			catch (System.Exception ex)
			{
				Program.Log(string.Format(pathMasksSaveError_key, PathMasksPath, ex.Message),
								pathMasksSaveErrorTitle_key);

				MessageBox.Show(string.Format(pathMasksSaveError_key, PathMasksPath, ex.Message),
								pathMasksSaveErrorTitle_key,
								MessageBoxButtons.OK,
								MessageBoxIcon.Error,
								MessageBoxDefaultButton.Button1);
			}
			finally
			{
				if (writer != null)
					writer.Close();
			}
		}
	}
}
