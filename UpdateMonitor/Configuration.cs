﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Sprocket.UpdateMonitor
{
	[Serializable]
	public class Configuration : IDisposable
	{
		private const string restoreBackup_key = "A backup file has been found with the targetted name '{0}'. Would you like to restore it? (It will otherwise be discarded.)";
		private const string restoreBackupTitle_key = "Restore Backup";

		private const string configFolder_key = "configs";
		private const string configFile_key = "{0}.xml";
		private string configLocation_key
		{
			get
			{
				return Path.Combine(Program.AppDataPath, configFolder_key, configFile_key);
			}
		}

		public string name;

		[NonSerialized]
		[XmlIgnore]
		public SyncManager syncManager = null;

		public string Name
		{
			get
			{
				return name;
			}
		}

		public Configuration()
		{
			name = string.Empty;

			syncManager = new SyncManager();
		}

		public Configuration (string _name)
		{
			name = _name;

			syncManager = new SyncManager();
		}

		public void SyncManagerLoad()
		{
			syncManager.LoadSyncList(string.Format(configLocation_key, name));
		}

		public void SyncManagerSave()
		{
			syncManager.SaveSyncList(string.Format(configLocation_key, name));
		}

		public void DisableConfigFile()
		{
			var originalFile = string.Format(configLocation_key, name);
			var backupFile = Path.ChangeExtension(originalFile, "bkp");

			if (File.Exists(originalFile))
			{
				File.Move(originalFile, backupFile);
			}
		}

		public void TryRestoreConfigFile()
		{
			var originalFile = string.Format(configLocation_key, name);
			var backupFile = Path.ChangeExtension(originalFile, "bkp");

			if (File.Exists(backupFile))
			{
				var result = MessageBox.Show(string.Format(restoreBackup_key, backupFile), restoreBackupTitle_key, MessageBoxButtons.YesNo);

				if (result == DialogResult.No)
				{
					File.Delete(backupFile);
				}
				else if (result == DialogResult.Yes)
				{
					File.Move(backupFile, originalFile);
				}
			}
		}

		public void Dispose()
		{
			syncManager.Dispose();
		}

		internal bool IsItemEnabled(string key)
		{
			return syncManager.IsEnabled(key);
		}

		internal void SetItemEnabled(string key, bool value)
		{
			syncManager.SetEnabled(key, value);
		}
	}
}
