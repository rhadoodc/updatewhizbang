﻿using Sprocket.UpdateMonitor.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sprocket.UpdateMonitor
{
	public partial class PathMasks : Form
	{
		public PathMasks()
		{
			InitializeComponent();

			modeComboBox.Items.Clear();

			int index = 0;

			foreach (PathMaskManager.MaskMode value in Enum.GetValues(typeof(PathMaskManager.MaskMode)))
			{
				modeComboBox.Items.Add(value.ToString());

				if (value == PathMaskManager.Instance.CurrentMode)
					modeComboBox.SelectedIndex = index;

				index++;
			}
		}

		private List<CheckBox> useRegexCheckBoxes = new List<CheckBox>();
		private List<CheckBox> enabledCheckBoxes = new List<CheckBox>();
		private List<Button> removeMaskButtons = new List<Button>();
		private List<Label> maskLabels = new List<Label>();
		public List<TextBox> maskTextBoxes = new List<TextBox>();

		const string invalidTargetPath_key = "The target mask is invalid: {0}";

		const string maskLabel_key = "Mask [{0}]";

		const string confirmRemoveMaskTitle_Key = "Remove Mask";
		const string confirmRemoveMask_Key = "Are you sure you want to remove this mask?\n\n    {0}";

		const int controlLineSize = 24;
		const int controlLineMargin = 5;

		readonly Size regexCheckBoxSize = new Size(52, controlLineSize);
		readonly Size enabledCheckBoxSize = new Size(16, controlLineSize);
		readonly Size removeMaskButtonSize = new Size(controlLineSize, controlLineSize);
		readonly Size maskTextBoxSize = new Size(190, controlLineSize);
		readonly Size maskLabelSize = new Size(55, controlLineSize);

		const int regexCheckBoxXOffset = 260;
		const int enabledCheckBoxXOffset = 4;
		const int removeMaskButtonXOffset = 315;
		const int maskTextBoxXOffset = 70;
		const int maskLabelXOffset = 20;

		const int maskLayoutBaseline = 45;

		private void cancelButton_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.Cancel;

			Close();
		}

		private void applyButton_Click(object sender, EventArgs e)
		{
			DialogResult = System.Windows.Forms.DialogResult.OK;

			var masks = new List<PathMaskManager.Mask>();

			for (int index = 0; index < maskTextBoxes.Count; index++)
			{
				var mask = new PathMaskManager.Mask();
				mask.maskString = maskTextBoxes[index].Text;
				mask.isRegex = useRegexCheckBoxes[index].Checked;
				mask.enabled = enabledCheckBoxes[index].Checked;

				masks.Add(mask);
			}

			PathMaskManager.Instance.SetMasks(masks);

			PathMaskManager.SaveMasks(Program.PathMasksPath);

			Close();
		}

		private void maskTextBox_TextChanged(object sender, EventArgs e)
		{
			this.Validate();
		}

		public void ConstructMaskField(int index, PathMaskManager.Mask? mask = null)
		{
			if (index < 0)
			{
				index = maskTextBoxes.Count;
			}

			//mask text box
			var maskTextBox = new TextBox();

			maskTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));

			maskTextBox.Size = maskTextBoxSize;
			maskTextBox.Location = new Point(maskTextBoxXOffset, maskLayoutBaseline + (controlLineSize + controlLineMargin) * index);

			maskTextBox.TextChanged += maskTextBox_TextChanged;

			maskTextBox.Validating += (object sender, CancelEventArgs e) =>
			{
				//try
				//{
				//	var path = Path.GetFullPath(maskTextBox.Text);
				//}
				//catch (System.Exception ex)
				//{
				//	e.Cancel = true;

				//	if (applyButton.Enabled == true)
				//	{
				//		applyButton.Enabled = false;

				//		errorProvider.Icon = Resources.warning_icon;
				//		errorProvider.SetIconPadding(maskTextBox, 5);
				//		errorProvider.SetError(maskTextBox, string.Format(invalidTargetPath_key, ex.Message));
				//	}
				//}
			};

			//mask label
			var maskLabel = new Label();

			maskLabel.Text = string.Format(maskLabel_key, index);

			maskLabel.Size = maskLabelSize;
			maskLabel.Location = new Point(maskLabelXOffset, maskLayoutBaseline + 3 + (controlLineSize + controlLineMargin) * index);

			//enabled check box
			var enabledCheckBox = new CheckBox();

			enabledCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
			| System.Windows.Forms.AnchorStyles.Right)));

			enabledCheckBox.Size = enabledCheckBoxSize;
			enabledCheckBox.Location = new Point(enabledCheckBoxXOffset, maskLayoutBaseline - 2 + (controlLineSize + controlLineMargin) * index);

			enabledCheckBox.CheckedChanged += (object sender, EventArgs e) =>
			{
				SetMaskEnabled(maskLabel, enabledCheckBox.Checked);
			};

			//treat mask as regex button
			var useRegexCheckBox = new CheckBox();

			useRegexCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));

			useRegexCheckBox.Text = "regex";
			useRegexCheckBox.CheckAlign = ContentAlignment.MiddleRight;

			useRegexCheckBox.Size = regexCheckBoxSize;
			useRegexCheckBox.Location = new Point(regexCheckBoxXOffset, maskLayoutBaseline - 2 + (controlLineSize + controlLineMargin) * index);

			useRegexCheckBox.CheckedChanged += (object sender, EventArgs e) => 
			{
				//folderBrowserDialog.SelectedPath = (maskTextBox.Text == string.Empty) ? lastTargetPath : maskTextBox.Text;
				//var result = folderBrowserDialog.ShowDialog();

				//if (result == DialogResult.OK)
				//{
				//	lastTargetPath = folderBrowserDialog.SelectedPath;
				//	maskTextBox.Text = folderBrowserDialog.SelectedPath;
				//}
			};

			//remove mask button
			var removeMaskButton = new Button();

			removeMaskButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));

			removeMaskButton.Image = Resources.targetRemove;
			removeMaskButton.UseVisualStyleBackColor = true;

			removeMaskButton.Size = removeMaskButtonSize;
			removeMaskButton.Location = new Point(removeMaskButtonXOffset, maskLayoutBaseline - 2 + (controlLineSize + controlLineMargin) * index);

			removeMaskButton.Click += (object sender, EventArgs e) =>
			{
				if (MessageBox.Show(string.Format(confirmRemoveMask_Key, maskTextBox.Text), confirmRemoveMaskTitle_Key, MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					RemoveMask(maskLabel);
				}
			};

			maskTextBoxes.Insert(index, maskTextBox);
			maskLabels.Insert(index, maskLabel);
			useRegexCheckBoxes.Insert(index, useRegexCheckBox);
			enabledCheckBoxes.Insert(index, enabledCheckBox);
			removeMaskButtons.Insert(index, removeMaskButton);
			
			if (mask != null)
			{
				maskTextBox.Text = mask.Value.maskString;
				useRegexCheckBox.Checked = mask.Value.isRegex;
				enabledCheckBox.Checked = mask.Value.enabled;

				SetMaskEnabled(maskLabel, mask.Value.enabled);
			}

			Controls.Add(maskTextBox);
			Controls.Add(maskLabel);
			Controls.Add(enabledCheckBox);
			Controls.Add(useRegexCheckBox);
			Controls.Add(removeMaskButton);

			toolTip.SetToolTip(enabledCheckBox, "Enable use of this mask.");
			toolTip.SetToolTip(useRegexCheckBox, "Treat this mask as a Regular Expression.");
			toolTip.SetToolTip(removeMaskButton, "Remove this mask field.");

			this.Size = new Size(this.Size.Width, this.Size.Height + controlLineMargin + controlLineSize);
		}

		private void SetMaskEnabled(Label label, bool value)
		{
			var index = maskLabels.IndexOf(label);

			var regexCheckBoxButton = useRegexCheckBoxes[index];
			var maskLabel = maskLabels[index];
			var maskTextBox = maskTextBoxes[index];

			regexCheckBoxButton.Enabled = value;
			maskLabel.Enabled = value;
			maskTextBox.Enabled = value;
		}

		private void RemoveMask(Label label)
		{
			var index = maskLabels.IndexOf(label);

			var removeMaskButton = removeMaskButtons[index];
			var regexCheckBoxButton = useRegexCheckBoxes[index];
			var maskLabel = maskLabels[index];
			var maskTextBox = maskTextBoxes[index];
			var enabledCheckBox = enabledCheckBoxes[index];

			Controls.Remove(enabledCheckBox);
			Controls.Remove(removeMaskButton);
			Controls.Remove(regexCheckBoxButton);
			Controls.Remove(maskLabel);
			Controls.Remove(maskTextBox);

			enabledCheckBoxes.Remove(enabledCheckBox);
			removeMaskButtons.Remove(removeMaskButton);
			useRegexCheckBoxes.Remove(regexCheckBoxButton);
			maskLabels.Remove(maskLabel);
			maskTextBoxes.Remove(maskTextBox);

			ReflowTargetFields();

			this.Size = new Size(this.Size.Width, this.Size.Height - controlLineMargin - controlLineSize);
		}

		private void ReflowTargetFields()
		{
			for (int index = 0; index < maskTextBoxes.Count; index ++)
			{
				var removeMaskButton = removeMaskButtons[index];
				var regexCheckBoxButton = useRegexCheckBoxes[index];
				var maskLabel = maskLabels[index];
				var maskTextBox = maskTextBoxes[index];
				var enabledCheckBox = enabledCheckBoxes[index];

				maskLabel.Text = string.Format(maskLabel_key, index);
				enabledCheckBox.Location = new Point(enabledCheckBoxXOffset, maskLayoutBaseline - 2 + (controlLineSize + controlLineMargin) * index);
				maskLabel.Location = new Point(maskLabelXOffset, maskLayoutBaseline + (controlLineSize + controlLineMargin) * index);
				removeMaskButton.Location = new Point(removeMaskButtonXOffset, maskLayoutBaseline - 2 + (controlLineSize + controlLineMargin) * index);
				regexCheckBoxButton.Location = new Point(regexCheckBoxXOffset, maskLayoutBaseline - 2 + (controlLineSize + controlLineMargin) * index);
				maskTextBox.Location = new Point(maskTextBoxXOffset, maskLayoutBaseline + (controlLineSize + controlLineMargin) * index);
			}
		}

		private void addMaskButton_Click(object sender, EventArgs e)
		{
			ConstructMaskField(-1);
		}

		private void PathMasks_Shown(object sender, EventArgs e)
		{
			foreach(var mask in PathMaskManager.Instance.masks)
			{
				ConstructMaskField(-1, mask);
			}

			if (maskTextBoxes.Count == 0)
			{
				ConstructMaskField(-1);
			}
		}

		private void modeComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			PathMaskManager.Instance.CurrentMode = (PathMaskManager.MaskMode)Enum.Parse(typeof(PathMaskManager.MaskMode), modeComboBox.SelectedItem.ToString());
		}
	}
}
