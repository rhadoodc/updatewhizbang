﻿namespace Sprocket.UpdateMonitor
{
	partial class PathMasks
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.addMaskButton = new System.Windows.Forms.Button();
			this.applyButton = new System.Windows.Forms.Button();
			this.cancelButton = new System.Windows.Forms.Button();
			this.modeComboBox = new System.Windows.Forms.ComboBox();
			this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this.toolTip = new System.Windows.Forms.ToolTip(this.components);
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
			this.SuspendLayout();
			// 
			// addMaskButton
			// 
			this.addMaskButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.addMaskButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.addMaskButton.Image = global::Sprocket.UpdateMonitor.Properties.Resources.add;
			this.addMaskButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.addMaskButton.Location = new System.Drawing.Point(3, 46);
			this.addMaskButton.Name = "addMaskButton";
			this.addMaskButton.Size = new System.Drawing.Size(84, 23);
			this.addMaskButton.TabIndex = 1;
			this.addMaskButton.Text = "Add Mask";
			this.addMaskButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.toolTip.SetToolTip(this.addMaskButton, "Add a new mask field.");
			this.addMaskButton.UseVisualStyleBackColor = true;
			this.addMaskButton.Click += new System.EventHandler(this.addMaskButton_Click);
			// 
			// applyButton
			// 
			this.applyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.applyButton.Location = new System.Drawing.Point(233, 46);
			this.applyButton.Name = "applyButton";
			this.applyButton.Size = new System.Drawing.Size(50, 23);
			this.applyButton.TabIndex = 2;
			this.applyButton.Text = "Apply";
			this.toolTip.SetToolTip(this.applyButton, "Apply Changes");
			this.applyButton.UseVisualStyleBackColor = true;
			this.applyButton.Click += new System.EventHandler(this.applyButton_Click);
			// 
			// cancelButton
			// 
			this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.Location = new System.Drawing.Point(289, 46);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(50, 23);
			this.cancelButton.TabIndex = 3;
			this.cancelButton.Text = "Cancel";
			this.toolTip.SetToolTip(this.cancelButton, "Cancel Changes");
			this.cancelButton.UseVisualStyleBackColor = true;
			this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
			// 
			// modeComboBox
			// 
			this.modeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.modeComboBox.FormattingEnabled = true;
			this.modeComboBox.Items.AddRange(new object[] {
            "Masks Disregarded",
            "Disable Masked Paths",
            "Enable Only Masked Paths"});
			this.modeComboBox.Location = new System.Drawing.Point(3, 13);
			this.modeComboBox.Name = "modeComboBox";
			this.modeComboBox.Size = new System.Drawing.Size(336, 21);
			this.modeComboBox.TabIndex = 0;
			this.toolTip.SetToolTip(this.modeComboBox, "Select the mode for handling masks.");
			this.modeComboBox.SelectedIndexChanged += new System.EventHandler(this.modeComboBox_SelectedIndexChanged);
			// 
			// errorProvider
			// 
			this.errorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
			this.errorProvider.ContainerControl = this;
			this.errorProvider.RightToLeft = true;
			// 
			// PathMasks
			// 
			this.AcceptButton = this.applyButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.cancelButton;
			this.ClientSize = new System.Drawing.Size(342, 81);
			this.Controls.Add(this.modeComboBox);
			this.Controls.Add(this.addMaskButton);
			this.Controls.Add(this.applyButton);
			this.Controls.Add(this.cancelButton);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "PathMasks";
			this.ShowInTaskbar = false;
			this.Text = "PathMasks";
			this.Shown += new System.EventHandler(this.PathMasks_Shown);
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button addMaskButton;
		private System.Windows.Forms.Button applyButton;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.ComboBox modeComboBox;
		private System.Windows.Forms.ErrorProvider errorProvider;
		private System.Windows.Forms.ToolTip toolTip;
	}
}